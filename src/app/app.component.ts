import { Component, Pipe } from '@angular/core';
// import { FileUploadModule } from 'ng2-file-upload';
// import { AngularFirestore } from '@angular/fire/firestore';
// import {
//   AngularFireStorage,
//   AngularFireStorageReference,
//   AngularFireUploadTask
// } from '@angular/fire/storage';
// import { catchError, finalize } from 'rxjs/operators';
// import { throwError } from 'rxjs';
// import {Pipe} from 'angular2/core';

@Pipe({name: 'round'})
export class RoundPipe {
  transform (input:number) {
    return Math.floor(input);
  }
}

export class NgxQrCode {
  text: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

    public qrdata: string = null;
    public qrDataList: any = [];
    public deviceWidth :number;
    public filetitle;
 csvContent: string;
 heightAndWidth:number;
 blockSize:number;
 totalElementsPerBlock:number;
 noOfBlocks:number;
 fileName: string;
 public fileString;

public downloadURL: string[] = [];
//fileToUpload: File = null;
//uploader: FileUploader = new FileUploader({ url: "api/your_upload", removeAfterUpload: false, autoUpload: true });
  constructor() {
    console.log('AppComponent running');
    this.fileString;
    this.heightAndWidth = 105;
    this.deviceWidth = window.innerWidth;
    this.blockSize = (this.deviceWidth/this.heightAndWidth);
    this.totalElementsPerBlock = Math.floor((this.deviceWidth/this.heightAndWidth));
    this.noOfBlocks = 10; // this.totalElementsPerBlock;
    //this.qrDataList = [];


  }
  onFileLoad(fileLoadedEvent) {
      const textFromFileLoaded = fileLoadedEvent.target.result;
      this.csvContent = textFromFileLoaded;
      alert(this.csvContent);
    }

  changeValue(newValue: string): void {
    this.qrdata = newValue;
  }
  changeHeightAndWidth($event): void {
    console.log("event.target.value:"+event.target["value"])
    this.heightAndWidth = event.target["value"];
    this.blockSize = (this.deviceWidth/this.heightAndWidth);
    this.totalElementsPerBlock = 10; //Math.floor((this.deviceWidth/this.heightAndWidth));
  }
  changeNoOfBlocks($event): void {
    console.log("event.target.value:"+event.target["value"])
    this.noOfBlocks = event.target["value"];
  }
  uploadFile(files) {
    for(let i =0; i < files.length; i++){
      //formData.append("files", files[i], files[i]['name']);
      console.log('files', files[i]['name']);
      const fileReader = new FileReader();
     fileReader.onload = this.onFileLoad;

     fileReader.readAsText(files[i]['name'], "UTF-8");

    }


  }
  changeListener($event): void {
            this.readThis($event.target);
        }

    readThis(inputValue: any): void {
        var file: File = inputValue.files[0];
        this.filetitle = this.fileName; //  (inputValue.files[0]['name'] as string).replace(/_/g, "").replace(".csv", "").toUpperCase();
        var myReader: FileReader = new FileReader();
        var fileType = inputValue.parentElement.id;
        var that = this;
        var t = { "short_name":"dumy", "license_key":"12345"};
        for(let i=0; i<100; i++){
          this.qrDataList.push(t);
        }
        myReader.onloadend = function (e) {
            const csvSeparator = ',';
         const textFromFileLoaded = myReader.result;
         var csvContent = textFromFileLoaded;

         const txt = textFromFileLoaded as string;
         const csv = [];
         const lines = txt.split('\n');
         var lineIndex = 0;
         var licenseKeyIndex = 1;
         var shortNameIndex = 0;

         lines.forEach(element => {
           const cols: string[] = element.split(csvSeparator);
           let temp_index = 0;
           if(lineIndex===0){
             for(let col in cols ){
               console.log("col:"+col)
               if(col==="License key"){
                 licenseKeyIndex = temp_index;
               }else if(col==="Short name"){
                 shortNameIndex = temp_index;
               }
             }
           }else{
             //this.qrdata = newValue;
             var temp = { "short_name":cols[shortNameIndex] , "license_key": cols[licenseKeyIndex]};
console.log("temp:"+temp)
             //debugger;
             that.qrDataList.push(temp);
             //csv.push(cols);
           }
           lineIndex = lineIndex+1;
         });
        // debugger;
        /// this.parsedCsv = csv;
         //console.log("chsv:"+csv)
            // var list = myReader;
            // debugger;
            // for(var i =0 ;i<list.result.length; i++){
            //   console.log("line:"+list.result[i])
            // }

            //fileString = myReader.result would not work,
            //because it is not in the scope of the callback
        }

        myReader.readAsText(file);
        let _element:  HTMLElement = <HTMLElement>document.getElementById("printButton");
        _element.style.display = "";
        _element= <HTMLElement>document.getElementById("inputBlock");
        _element.style.display = "none";
    }
    printQuestionPaper() {
      // const printContent = document.getElementById("content3");
      // const WindowPrt = window.open('', '', 'left=12.5, bottom=10, top=5,toolbar=0,scrollbars=0,status=0');
      // WindowPrt.document.write(printContent.innerHTML);
      // WindowPrt.document.close();
      // WindowPrt.focus();
      // WindowPrt.print();
      // WindowPrt.close();

      let _element:  HTMLElement = <HTMLElement>document.getElementById("printButton");
      _element.style.display = "none";
      window.print();
      window.setTimeout(() => {
        _element = <HTMLElement>document.getElementById("printButton");
        _element.style.display = "";
      }, 10000);
     //   let that = this;
     //   var printContents = document.getElementById("content3").innerHTML;
     //   let _element:  HTMLElement = <HTMLElement>document.body.children[1];
     //   _element.style.display = "none";
     //   let elment = document.createElement('div');
     //   elment.innerHTML = printContents;
     //   document.body.appendChild(elment);
     //   //this.closePreview();
     //
     //   window.print();
     //   window.setTimeout(() => {
     //  document.body.removeChild(elment);
     //     let _element2: HTMLElement = <HTMLElement>document.body.children[1];
     //     _element2.style.display = "";
     //   }, 100);
     }
}
